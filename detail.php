<?php include("header.php"); ?>
<div id="container">
	<div class="submenu">
		<ol>
			<li><a href="index.php">Beranda</a></li>
			<li>/</li>
			<li><a href="detail.php">Berita</a></li>
			<li>/</li>
			<li class="active">Koarmatim Bina Preman dan Pengangguran Wilayah Surabaya</li>
		</ol>
	</div>
	<div class="left-detail detail">
		<div class="thumb">
			<img src="http://kominfo.jatimprov.go.id/uploads/photonews/16/03/a020af6ad4ae8a6830e3387d6959f4c2.jpg">
			<a href="detail.php"><h2>Koarmatim Bina Preman dan Pengangguran Wilayah Surabaya</h2></a>
			<div class="sub">Senin, 22 Februari 2016</div>
			<p>Jatim Newsroom - Komando Armada RI Kawasan Timur (Koarmatim) menggelar pembinaan ketahanan wilayah yang diikuti 100 peserta, terdiri dari para preman dan pengangguran di wilayah Surabaya, Senin (21/3).

Usai penyematan tanda peserta, Panglima Komando Armada RI Kawasan Timur (Pangarmatim),Laksamana Muda TNI Darwanto mengatakan, sistem pertahanan Negara Kesatuan Republik Indonesia (NKRI) adalah Sistem Pertahanan Semesta (Sishanta) yang melibatkan seluruh warga negara, wilayah dan sumber daya lainnya.

Dengan dasar itulah,pemerintah melalui TNI memiliki tugas menyiapkan secara dini dan menyelenggarakan secara total, terpadu, terarah dan berlanjut untuk menegakkan kedaulatan negara, keutuhan wilayah dan keselamatan segenap bangsa dari segala ancamansesuai dengan UUNo 34/2004 tentang TNI, pada pasal 7 (2) huruf b angka 8 yaitu TNI bertugas memberdayakan wilayah pertahahan dan kekuatan pendukungnya secara dini sesuai dengan Sishanta.

“Pembinaan ketahanan wilayah yang dilaksanakan di Koarmatim ini, seluruh peserta akan diasramakan di mess Kolat Koarmatim selama 30 hari, untuk mengikuti seluruh agenda kegitan,” katanya.

Pembinaan ketahanan wilayah ini bertujuan untuk meningkatkan wawasan kebangsaan untuk membangun generasi muda yang disiplin, semangat, kreatif, inovatif dan bermartabat serta bermanfaat untuk masyarakat, bangsa dan negara. “Melalui program ini diharapkan dapat membentuk sikap mental yang baik, memiliki rasa cinta tanah air, kesadaran berbangsa dan bernegara serta memiliki kemampuan awal bela negara,” terangnya.

Pangarmatim berharap setelah mengikuti kegiatan ini seluruh peserta memiliki disiplin, semangat, etos kerja yang tinggi, berwawasan kebangsaan memiliki kemampuan awal untuk bela negara  secara psychis maupun fisik.“Para peserta akan di diarahkan sebagai tenaga-tenaga kerja yang siap pakai, kegiatan ini juga sebagai bentuk perhatian TNI kepada masyarakat,” tutur Pangarmatim.

Acara pembukaan yang juga diramaikan dengan demonstrasi Karate, Taekwondo, dan pencak silat oleh prajurit Koarmatim ini dihadiri Kasarmatim Laksma TNI Mintoro Yulianto, para Pejabat Utama Koarmatim, para Komandan KRI yang berada di pangkalan, Kasatpol PP Tanjung Perak, perwakilan Kesbang Linmas Kota Surabaya, serta dr. David yang siap membantu menyalurkan mantan peserta pembinaan ke lapangan-lapangan pekerjaan. Usai melaksanakan upacara pembukaan para peserta Bintahwil mengikuti program kunjungan ke KRI Dewaruci yang sandar di Dermaga Koarmatim.(hjr)</p>
		</div>
	</div>
	<div class="right-detail">
		<div class="blockq">
			<div class="container">
				<h3>Berita Terpopuler</h3>
			</div>
		</div>
		<div class="populer">
			<div class="sub">Senin, 30 Maret 2016</div>
			<a href="#"><p>Ribuan Goweser Padati Ruas Tol Gempan </p></a>
			<div class="sub">Kamis, 12 Maret 2016</div>
			<a href="#"><p>EVALUASI SWASEMBADA BERAS, PANGDAM V BRAWIJAYA BERKUNJUNG KE KODIM 0819 PASURUAN </p></a>
			<div class="sub">Senin, 23 Februari 2016</div>
			<a href="#"><p>SMKN I GRATI SUKSES ANTARKAN 90% SISWANYA, LANGSUNG KERJA </p></a>
			<div class="sub">Senin, 23 Februari 2016</div>
			<a href="#"><p>Kabupaten Pasuruan Siap Jadi Tuan Rumah Final East Java Scouts Challenge 2K15 </p></a>
			<div class="sub">Selasa, 18 November 2016</div>
			<a href="#"><p>BBM BELUM NAIK, HARGA CABAI KERITING SUDAH TEMBUS Rp 50.000 </p></a>
			<div class="sub">Kamis, 25 September 2016</div>
			<a href="#"><p>TINDAK TEGAS OKNUM YANG "BERMAIN" DALAM REKRUITMEN CPNS </p></a>
			<div class="sub">Kamis, 25 September 2014</div>
			<a href="#"><p>IRSYAD BERANGKATKAN 820 TAMU ALLAHKAN AIR BERSIH </p></a>
			<div class="sub">Kamis, 25 September 2014</div>
			<a href="#"><p>KERING KRITIS, PEMKAB PASURUAN TERUS DISTRIBUSIKAN AIR BERSIH </p></a>
		</div><br>
		<div class="blockq">
			<div class="container">
				<h3>Agenda & Kegiatan</h3>
			</div>
		</div>
		<div class="populer">
			<a href="#"><p>Pertemuan Bupati Pasuruan</p></a>
			<a href="#"><p>Workshop Internet Sehat</p></a>
			<a href="#"><p>Lomba Website Smart e-government</p></a>
			<a href="#"><p>Seminar Nasional Teknologi</p></a>
			<a href="#"><p>Pertemuan Gubernur Pasuruan</p></a>
		</div><br>
		<div class="clear"></div>
		<div class="blockq">
			<div class="container">
				<h3>Top Download</h3>
			</div>
		</div>
		<div class="populer">
			<a href="#"><p>Renstra Diskominfo 2013-2018 (982)</p></a>
			<a href="#"><p>LAKIP Diskominfo 2013 (240)</p></a>
			<a href="#"><p>LAKIP Diskominfo 2014 (139)</p></a>
			<a href="#"><p>LAKIP Diskominfo 2015 (20)</p></a>
			<a href="#"><p>LAKIP Diskominfo 2016 (10)</p></a>
		</div>
	</div>
	<div class="clear"></div>
</div>
<?php include("footer.php"); ?>