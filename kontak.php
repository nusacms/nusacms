<?php include('header.php'); ?>
<div id="container">
	<div class="submenu">
		<ol>
			<li><a href="index.php">Beranda</a></li>
			<li>/</li>
			<li>Kontak Kami</li>
		</ol>
	</div>
	<div class="left-detail detail">
		<p>Anda bisa menghubungi kami melalui form online yang disediakan di bawah ini. 
Semua pesan yang Anda tulis,<br> akan terkirim ke E-mail Admin. 
<br>*Mohon melampirkan E-Mail Yang Valid. 
Terimakasih.</p>
		<form action="kontak.php">
			<input class="input" type="text" placeholder="Nama Depan" required><br>
			<input class="input" type="text" placeholder="Nama Belakang" required><br>
			<input class="input" type="email" placeholder="Email" required><br>
			<textarea placeholder="Pesan Anda" required></textarea><br>
			<input class="submit" type="submit" value="Kirim">
		</form><br>
	</div>
	<div class="right-detail">
		<div class="blockq">
			<div class="container">
				<h3>Berita Terpopuler</h3>
			</div>
		</div>
		<div class="populer">
			<div class="sub">Senin, 30 Maret 2016</div>
			<a href="#"><p>Ribuan Goweser Padati Ruas Tol Gempan </p></a>
			<div class="sub">Kamis, 12 Maret 2016</div>
			<a href="#"><p>EVALUASI SWASEMBADA BERAS, PANGDAM V BRAWIJAYA BERKUNJUNG KE KODIM 0819 PASURUAN </p></a>
			<div class="sub">Senin, 23 Februari 2016</div>
			<a href="#"><p>SMKN I GRATI SUKSES ANTARKAN 90% SISWANYA, LANGSUNG KERJA </p></a>
			<div class="sub">Senin, 23 Februari 2016</div>
			<a href="#"><p>Kabupaten Pasuruan Siap Jadi Tuan Rumah Final East Java Scouts Challenge 2K15 </p></a>
			<div class="sub">Selasa, 18 November 2016</div>
			<a href="#"><p>BBM BELUM NAIK, HARGA CABAI KERITING SUDAH TEMBUS Rp 50.000 </p></a>
			<div class="sub">Kamis, 25 September 2016</div>
			<a href="#"><p>TINDAK TEGAS OKNUM YANG "BERMAIN" DALAM REKRUITMEN CPNS </p></a>
			<div class="sub">Kamis, 25 September 2014</div>
			<a href="#"><p>IRSYAD BERANGKATKAN 820 TAMU ALLAHKAN AIR BERSIH </p></a>
			<div class="sub">Kamis, 25 September 2014</div>
			<a href="#"><p>KERING KRITIS, PEMKAB PASURUAN TERUS DISTRIBUSIKAN AIR BERSIH </p></a>
		</div><br>
		<div class="blockq">
			<div class="container">
				<h3>Agenda & Kegiatan</h3>
			</div>
		</div>
		<div class="populer">
			<a href="#"><p>Pertemuan Bupati Pasuruan</p></a>
			<a href="#"><p>Workshop Internet Sehat</p></a>
			<a href="#"><p>Lomba Website Smart e-government</p></a>
			<a href="#"><p>Seminar Nasional Teknologi</p></a>
			<a href="#"><p>Pertemuan Gubernur Pasuruan</p></a>
		</div><br>
		<div class="clear"></div>
		<div class="blockq">
			<div class="container">
				<h3>Top Download</h3>
			</div>
		</div>
		<div class="populer">
			<a href="#"><p>Renstra Diskominfo 2013-2018 (982)</p></a>
			<a href="#"><p>LAKIP Diskominfo 2013 (240)</p></a>
			<a href="#"><p>LAKIP Diskominfo 2014 (139)</p></a>
			<a href="#"><p>LAKIP Diskominfo 2015 (20)</p></a>
			<a href="#"><p>LAKIP Diskominfo 2016 (10)</p></a>
		</div>
	</div>
	<div class="clear"></div>
</div>
<?php include('footer.php'); ?>