<?php include("header.php"); ?>
<div class="content">
	<div id="container">
		<div class="left-detail">
			<div class="blockq">
				<div class="container">
					<div class="text-left"><h3>Berita Terbaru</h3></div>
					<div class="text-right"><h3><a href="selengkapnya.php">Selengkapnya</a></h3></div>
				</div>
			</div>
			<div class="left-detail">
				<div class="thumb">
					<a href="#"><img class="cover" src="http://kominfo.jatimprov.go.id/uploads/photonews/16/03/a020af6ad4ae8a6830e3387d6959f4c2.jpg"></a>
					<a href="detail.php"><h3>Koarmatim Bina Preman dan Pengangguran Wilayah Surabaya</h3></a>
					<div class="sub">Senin, 22 Februari 2016</div>
					<p><b>Jatim Newsroom</b> - Komando Armada RI Kawasan Timur (Koarmatim) menggelar pembinaan ketahanan wilayah yang diikuti 100 peserta, terdiri dari para preman dan pengangguran di wilayah Surabaya, Senin (21/3).
Usai penyematan tanda peserta, Panglima Komando Armada RI Kawasan Timur (Pangarmatim),Laksamana Muda TNI Darwanto mengatakan, ...</p>
				</div>
			</div>
			<div class="right-detail">
				<div class="thumb mini">
					<a href="#"><img src="http://diskominfo.pasuruankab.go.id/mod/news/images/normal/3c2c06d9381800463ee65a0104d8f6ee.jpg"></a>
					<a href="detail.php"><h3>EVALUASI SWASEMBADA BERAS</h3></a>
					<div class="sub">Senin, 22 Februari 2016</div>
				</div>
				<div class="thumb mini">
					<a href="#"><img src="http://diskominfo.pasuruankab.go.id/mod/news/images/normal/9e740602d18142ba588f6f6707f5ea06.jpg"></a>
					<a href="detail.php"><h3>SMKN I GRATI SUKSES ANTARKAN 90% SISWANYA</h3></a>
					<div class="sub">Senin, 22 Februari 2016</div>
				</div>
				<div class="thumb mini">
					<a href="#"><img src="http://diskominfo.pasuruankab.go.id/mod/news/images/normal/c9537075e33370508b55c18bb3fb8742.jpg"></a>
					<a href="detail.php"><h3>Kabupaten Pasuruan Siap Jadi Tuan Rumah</h3></a>
					<div class="sub">Senin, 22 Februari 2016</div>
				</div>
			</div>
			<div class="clear"></div>
			<!-- end -->
			<div class="blockq">
				<div class="container">
					<div class="text-left"><h3>Artikel Terbaru</h3></div>
					<div class="text-right"><h3><a href="selengkapnya.php">Selengkapnya</a></h3></div>
				</div>
			</div>
			<div class="col-left">
				<div class="thumb">
					<a href="#"><img src="http://www.rembangkab.go.id/images/joomlart/article/mod-1.jpg"></a>
					<a href="detail.php"><h3>Heading</h3></a>
					<div class="sub">Senin, 22 Februari 2016</div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
				</div>
			</div>
			<div class="col-middle">
				<div class="thumb">
					<a href="#"><img src="http://www.rembangkab.go.id/images/joomlart/article/mod-1.jpg"></a>
					<a href="detail.php"><h3>Heading</h3></a>
					<div class="sub">Senin, 22 Februari 2016</div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit..</p>
				</div>
			</div>
			<div class="col-right">
				<div class="thumb">
					<a href="#"><img src="http://www.rembangkab.go.id/images/joomlart/article/mod-1.jpg"></a>
					<a href="detail.php"><h3>Heading</h3></a>
					<div class="sub">Senin, 22 Februari 2016</div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
				</div>
			</div>
			<div class="clear"></div>
			<br>
			<div class="blockq">
				<div class="container">
					<div class="text-left"><h3>Galeri Foto</h3></div>
					<div class="text-right"><h3><a href="galeri.php">Selengkapnya</a></h3></div>
				</div>
			</div>
			<div class="col-img">
				<div class="thumb">
					<a href="#">
						<img src="http://kominfo.jatimprov.go.id/uploads/gallimages/3cc2048573952a225e585e316a362c0f.jpg">
						<div class="title">Gubernur Soekarwo menjelaskan tentang Konsep Jatimnomics</div>
					</a>
				</div>
			</div>
			<div class="col-img">
				<div class="thumb">
					<a href="#">
						<img src="http://kominfo.jatimprov.go.id/uploads/gallimages/94407abfbc09dad751441b66191fbb50.jpg">
						<div class="title">Gubernur Soekarwo menjelaskan tentang Konsep Jatimnomics</div>
					</a>
				</div>
			</div>
			<div class="col-img">
				<div class="thumb">
					<a href="#">
						<img src="http://kominfo.jatimprov.go.id/uploads/gallimages/eefb7c0263d927ed837bf61aac1864db.jpg">
						<div class="title">Gubernur Soekarwo menjelaskan tentang Konsep Jatimnomics</div>
					</a>	
				</div>
			</div>
			<div class="col-img">
				<div class="thumb">
					<a href="#">
						<img src="http://kominfo.jatimprov.go.id/uploads/gallimages/6ccfc55d92c001ce2ac0a6e567c1aa8c.jpg">
						<div class="title">Presiden RI , Joko Widodo, menghadiri Upacara Peringatan Hari Pahlawan</div>
					</a>
				</div>
			</div>
		</div>
		<div class="right-detail">
			<div class="blockq">
				<div class="container">
					<h3>Video</h3>
				</div>
			</div>
			<div class="thumb">
				<div class="video">
					<iframe src="http://www.youtube.com/embed/vF1WwkE6NqM" frameborder="0" width="100%" height="230"></iframe>
				</div>
			</div>
			<div class="clear"></div>
			<div class="blockq">
				<div class="container">
					<h3>Berita Terpopuler</h3>
				</div>
			</div>
			<div class="populer">
				<div class="sub">Senin, 30 Maret 2016</div>
				<a href="#"><p>Ribuan Goweser Padati Ruas Tol Gempan </p></a>
				<div class="sub">Kamis, 12 Maret 2016</div>
				<a href="#"><p>EVALUASI SWASEMBADA BERAS, PANGDAM V BRAWIJAYA BERKUNJUNG KE KODIM 0819 PASURUAN </p></a>
				<div class="sub">Senin, 23 Februari 2016</div>
				<a href="#"><p>SMKN I GRATI SUKSES ANTARKAN 90% SISWANYA, LANGSUNG KERJA </p></a>
				<div class="sub">Senin, 23 Februari 2016</div>
				<a href="#"><p>Kabupaten Pasuruan Siap Jadi Tuan Rumah Final East Java Scouts Challenge 2K15 </p></a>
				<div class="sub">Selasa, 18 November 2016</div>
				<a href="#"><p>BBM BELUM NAIK, HARGA CABAI KERITING SUDAH TEMBUS Rp 50.000 </p></a>
				<div class="sub">Kamis, 25 September 2016</div>
				<a href="#"><p>TINDAK TEGAS OKNUM YANG "BERMAIN" DALAM REKRUITMEN CPNS </p></a>
				<div class="sub">Kamis, 25 September 2014</div>
				<a href="#"><p>IRSYAD BERANGKATKAN 820 TAMU ALLAHKAN AIR BERSIH </p></a>
				<div class="sub">Kamis, 25 September 2014</div>
				<a href="#"><p>KERING KRITIS, PEMKAB PASURUAN TERUS DISTRIBUSIKAN AIR BERSIH </p></a>
			</div><br>
			<div class="clear"></div>
			<div class="blockq">
				<div class="container">
					<h3>Agenda & Kegiatan</h3>
				</div>
			</div>
			<div class="populer">
				<a href="#"><p>Pertemuan Bupati Pasuruan</p></a>
				<a href="#"><p>Workshop Internet Sehat</p></a>
				<a href="#"><p>Lomba Website Smart e-government</p></a>
				<a href="#"><p>Seminar Nasional Teknologi</p></a>
				<a href="#"><p>Pertemuan Gubernur Pasuruan</p></a>
			</div><br>
			<div class="clear"></div>
			<div class="blockq">
				<div class="container">
					<h3>Top Download</h3>
				</div>
			</div>
			<div class="populer">
				<a href="#"><p>Renstra Diskominfo 2013-2018 (982)</p></a>
				<a href="#"><p>LAKIP Diskominfo 2013 (240)</p></a>
				<a href="#"><p>LAKIP Diskominfo 2014 (139)</p></a>
				<a href="#"><p>LAKIP Diskominfo 2015 (20)</p></a>
				<a href="#"><p>LAKIP Diskominfo 2016 (10)</p></a>
			</div>
			<!-- isi -->
		</div>
	</div>
</div>
<div class="clear"></div>
<?php include("footer.php"); ?>