<!DOCTYPE html>
<html>
<head>
	<title>Dashboard Administrator - Kominfo Pasuruan</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<nav>
	<div class="container">
		<div class="nusacms">
			<div class="heading">
				<a href="home.php"><img class="logo" src="../img/logo.png"></a>
			</div>
		</div>
		<div class="logout">
			<div class="user">
				<b>Administrator</b>&nbsp;<a href="index.php"><img class="icon-admin" src="../img/icon/keluar.png"></a>
			</div>
		</div>
	</div>
</nav>
<div class="sidebar">
	<div class="menu">
		<ul>
			<li><a href="home.php"><img class="icon-left" src="../img/icon/home.png"><span>Dashboard</span><img class="icon-right" src="../img/icon/right.png"></a></li>
			<li><a href="#profil" class="drop"><img class="icon-left" src="../img/icon/profile2.png"><span>Profil</span><img class="icon-right" src="../img/icon/down.png"></a>
				<ul class="dropdown">
					<li><a href="#"><img class="icon-sub" src="../img/icon/radio.png"><span>Submenu</span><img class="icon-right" src="../img/icon/right.png"></a>
					<li><a href="#"><img class="icon-sub" src="../img/icon/radio.png"><span>Submenu</span><img class="icon-right" src="../img/icon/right.png"></a>
					<li><a href="#"><img class="icon-sub" src="../img/icon/radio.png"><span>Submenu</span><img class="icon-right" src="../img/icon/right.png"></a>
				</ul>
			</li>
			<li><a href="#"><img class="icon-left" src="../img/icon/berita.png"><span>Berita</span><img class="icon-right" src="../img/icon/right.png"></a></li>
			<li><a href="#"><img class="icon-left" src="../img/icon/download.png"><span>Download</span><img class="icon-right" src="../img/icon/right.png"></a></li>
			<li><a href="#"><img class="icon-left" src="../img/icon/agenda.png"><span>Agenda</span><img class="icon-right" src="../img/icon/right.png"></a></li>
			<li><a href="#"><img class="icon-left" src="../img/icon/event.png"><span>Events</span><img class="icon-right" src="../img/icon/right.png"></a></li>
			<li><a href="#"><img class="icon-left" src="../img/icon/galeri.png"><span>Galeri</span><img class="icon-right" src="../img/icon/right.png"></a></li>
		</ul>
	</div>
</div>
<br>