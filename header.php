<!DOCTYPE html>
<html>
<head>
	<title>Situs Dinas Kominfo Pasuruan</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
</head>
<body>
<nav>
<div id="container">
	<div class="logo">
		<a href="index.php"><img src="img/logo.png"></a>
	</div>
	<div class="menu-btn" id="menu-btn">
		<div></div>
		<span></span>
		<span></span>
		<span></span>
	</div>
	<div class="clear none"></div>
	<div class="responsive-menu">
		<div class="menu">
			<ul>
				<li><a href="index.php"><img class="icon" src="img/icon/home.png">Home</a></li>
				<li><a class="drop"><img class="icon" src="img/icon/profile2.png">Profil</a>
					<ul class="dropdown">
						<li><a href="profil.php"><img class="icon" src="img/icon/right.png">Gambaran Umum</a></li>
						<li><a href="profil.php"><img class="icon" src="img/icon/right.png">Struktur Organisasi</a></li>
						<li><a href="profil.php"><img class="icon" src="img/icon/right.png">Visi dan Misi</a></li>
						<li><a href="profil.php"><img class="icon" src="img/icon/right.png">RENSTRA Diskominfo</a></li>
					</ul>
				</li>
				<!-- <li><a href="#"><img class="icon" src="img/icon/berita.png">Berita</a></li> -->
				<!-- <li><a href="#"><img class="icon" src="img/icon/galeri.png">Galeri Foto</a></li> -->
				<li><a href="galeri.php"><img class="icon" src="img/icon/galeri.png">Galeri</a></li>
				<li><a class="drop2"><img class="icon" src="img/icon/layanan.png">Layanan</a>
					<ul class="dropdown2">
						<li><a href="layanan.php"><img class="icon" src="img/icon/right.png">Lembaga Penyiaran TV </a></li>
						<li><a href="layanan.php"><img class="icon" src="img/icon/right.png">Penyelenggaraan POS</a></li>
					</ul>
				</li>
				<li><a href="kontak.php"><img class="icon" src="img/icon/email.png">Kontak Kami</a></li>
			</ul>
		</div>
	</div>
</div>
</nav>
<div class="slideshow">
	<div>
		<img src="img/slider.jpg">
		<div class="caption change">
			<div class="h1">Rapat Dinas Kominfo Pasuruan</div>
		</div>
	</div>
	<div>
		<img src="img/bg1.jpg">
		<div class="caption">
			<div class="h1">Gunung Bromo Kabupaten Pasuruan</div>
		</div>
	</div>
	<div>
		<img src="img/bg3.jpg">
		<div class="caption">
			<div class="h1">Gunung Bromo Kabupaten Pasuruan (2)</div>
		</div>
	</div>
</div>
<div class="clear"></div>
<div class="content">
	<div class="well">
		<div id="container">
			<p>Selamat Datang di Website Resmi Dinas Kominfo Kabupaten Pasuruan</p>
		</div>
	</div>
</div>