<!-- <div class="footer">
	<div id="container">
		<div class="row">
			<h3>DINAS</h3>
			<a href="#"><p>Dinas Kebudayaan dan Pariwisata</p></a>
			<a href="#"><p>Dinas Kominfo</p></a>
			<a href="#"><p>Dinas Pendidikan</p></a>
			<a href="#"><p>Dinas Pol PP dan Linmas</p></a>
			<a href="#"><p>Dinas Kesehatan</p></a>
			<a href="#"><p>Dinas Pertanian</p></a>
		</div>
		<div class="row">
			<h3>BADAN</h3>
			<a href="#"><p>Inspektorat</p></a>
			<a href="#"><p>BAPPEDA</p></a>
			<a href="#"><p>Badan Lingkungan Hidup</p></a>
			<a href="#"><p>Badan Pemberdayaan Masyarakat</p></a>
			<a href="#"><p>Sekretariat DPRD</p></a>
			<a href="#"><p>Badan Kesatuan Bangsa & Politik</p></a>
		</div>
		<div class="row">
			<h3>KANTOR</h3>
			<a href="#"><p>KPU</p></a>
			<a href="#"><p>Ketahanan Pangan</p></a>
			<a href="#"><p>Kantor KB PP</p></a>
			<a href="#"><p>Kantor Perpustakaan dan Arsip</p></a>
			<a href="#"><p>BPBD</p></a>
		</div>
		<div class="row">
			<h3>KECAMATAN</h3>
			<a href="#"><p>Kecamatan Purwosari</p></a>
			<a href="#"><p>Kecamatan Bangil</p></a>
			<a href="#"><p>Kecamatan Pandaan</p></a>
			<a href="#"><p>Kecamatan Beji</p></a>
			<a href="#"><p>Kecamatan Wonorejo</p></a>
			<a href="#"><p>Kecamatan Kraton</p></a>
		</div>
	</div><div class="clear"></div>
</div>
 -->
<footer>
	<div id="container">
		Dinas Kominfo Kabupaten Pasuruan &copy; Copyrights 2015 - All Rights Reserved
	</div>
</footer>

<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".slideshow > div:gt(0)").hide();
	setInterval(function() { 
	  $('.slideshow > div:first')
	    .fadeOut(2000)
	    .next()
	    .fadeIn(2000)
	    .end()
	    .appendTo('.slideshow');
	},  4000);
});
$(document).ready(function(){       
	var scroll_start = 0;
	var startchange = $('.change');
	var offset = startchange.offset();
	if (startchange.length){
	$(document).scroll(function() { 
		scroll_start = $(this).scrollTop();
		if(scroll_start > offset.top) {
			$("nav, .dropdown, .dropdown2").css('background-color', '#2c3e50');
			$("nav").css('position', 'fixed');
		} else {
			$('nav, .dropdown, .dropdown2').css('background-color', 'rgba(0,0,0,0.1)');
			$("nav").css('position', 'absolute');
		}
	});
    }
});
$(document).ready(function(){
	$('.menu-btn').click(function(){
    $('.responsive-menu').slideToggle('0', function(){
		$('.responsive-menu').toggleClass('expand');
    });
	});
	$('.menu ul li .drop').click(function(){
		$('.menu ul li .dropdown').slideToggle(500);
	});
	$('.menu ul li .drop2').click(function(){
		$('.menu ul li .dropdown2').slideToggle(500);
	});
});

</script>
</body>
</html>